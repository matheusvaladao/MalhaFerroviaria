/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package malha;

import java.util.Objects;

/**
 *
 * @author Matheus
 */
public class Divisa {

    private Provincia provincia;
    private int distancia;

    public Divisa() {
    }

    public Divisa(Provincia provincia, int distancia) {
        this.provincia = provincia;
        this.distancia = distancia;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.provincia);
        hash = 31 * hash + this.distancia;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Divisa other = (Divisa) obj;
        if (this.distancia != other.distancia) {
            return false;
        }
        if (!Objects.equals(this.provincia, other.provincia)) {
            return false;
        }
        return true;
    }

}
