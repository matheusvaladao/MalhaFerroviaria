/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package malha;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Matheus
 */
public class Malha {

    private final List<Provincia> rotas;

    public Malha() {
        rotas = new LinkedList<>();
    }

    public List<Provincia> getRotas() {
        return rotas;
    }

    /**
     * Metodo responsavel por inserir uma rota na malha.
     *
     * @param malha
     */
    public void inserirMalha(String malha) {
        String rotasString[] = malha.split(Pattern.quote(","));

        for (String s : rotasString) {

            String origemString = s.trim().substring(0, 1);
            String destinoString = s.trim().substring(1, 2);
            int distancia = Integer.parseInt(s.trim().substring(2, 3));

            Provincia origem = new Provincia(origemString);
            Provincia destino = new Provincia(destinoString);

            Divisa divisa = new Divisa(destino, distancia);
            origem.setDivisa(divisa);
            rotas.add(origem);
        }
    }

    /**
     * Metodo que verifica existencia e calcula distancia caso exista trajeto.
     *
     * @param trajeto
     * @return
     */
    public String calcularDistancia(String trajeto) {
        int distancia = calcularDistanciaInteira(trajeto);
        if (distancia != 0) {
            return "" + distancia;
        } else {
            return "ROTA NAO EXISTE";
        }
    }

    /**
     * Metodo responsavel por calcular a distancia em um trajeto.
     *
     * @param trajeto
     * @return
     */
    private int calcularDistanciaInteira(String trajeto) {

        String[] pontosDoTrajeto = trajeto.split(Pattern.quote("-"));
        int distancia = 0;
        int quantidadeRotas = 0;

        for (int i = 0; i < pontosDoTrajeto.length - 1; i++) {
            for (int j = 0; j < rotas.size(); j++) {
                if (pontosDoTrajeto[i].trim().equals(rotas.get(j).getNome())) {
                    if (rotas.get(j).getDivisa().getProvincia().getNome().equals(pontosDoTrajeto[i + 1].trim())) {
                        distancia = distancia + rotas.get(j).getDivisa().getDistancia();
                        quantidadeRotas++;
                    }
                }
            }
        }
        if (quantidadeRotas == pontosDoTrajeto.length - 1) {
            return distancia;
        } else {
            return 0;
        }
    }

    /**
     * Metodo que obtem trajetos por quantidade maxima de paradas.
     *
     * @param origem
     * @param destino
     * @param paradasMaxima
     * @return
     */
    public int obterQuantidadeTrajetosPorParadasMaxima(String origem, String destino, int paradasMaxima) {
        return obterQuantidadeTrajetosPorParadasMaxima(origem, destino, paradasMaxima, 0, 0);
    }

    /**
     * Metodo que obtem trajetos por quantidade maxima de paradas. (Algoritmo)
     *
     * @param origem
     * @param destino
     * @param paradas
     * @param paradasAteProvincia
     * @return
     */
    private int obterQuantidadeTrajetosPorParadasMaxima(String origem, String destino, int paradasMaxima, int paradasAteProvincia, int quantidadeTrajetos) {

        List<String> proximaProvincia = new LinkedList<>();
        paradasAteProvincia++;

        for (int i = 0; i < rotas.size(); i++) {
            if (rotas.get(i).getNome().equals(origem)) {
                proximaProvincia.add(rotas.get(i).getDivisa().getProvincia().getNome());
            }
        }

        for (int i = 0; i < proximaProvincia.size(); i++) {

            if (paradasAteProvincia <= paradasMaxima) {

                if (proximaProvincia.get(i).equals(destino)) {
                    quantidadeTrajetos++;
                } else {
                    quantidadeTrajetos = Malha.this.obterQuantidadeTrajetosPorParadasMaxima(proximaProvincia.get(i), destino, paradasMaxima, paradasAteProvincia, quantidadeTrajetos);
                }

            }
        }

        return quantidadeTrajetos;
    }

    /**
     * Metodo que obtem trajetos por quantidade de paradas.
     *
     * @param origem
     * @param destino
     * @param paradas
     * @return
     */
    public int obterQuantidadeTrajetosPorParadas(String origem, String destino, int paradas) {
        return obterQuantidadeTrajetosPorParadas(origem, destino, paradas, 0, 0);
    }

    /**
     * Metodo que obtem trajetos por quantidade de paradas. (Algoritmo)
     *
     * @param origem
     * @param destino
     * @param paradas
     * @param paradasAteProvincia
     * @return
     */
    private int obterQuantidadeTrajetosPorParadas(String origem, String destino, int paradas, int paradasAteProvincia, int quantidadeTrajetos) {

        List<String> proximaProvincia = new LinkedList<>();
        paradasAteProvincia++;

        for (int i = 0; i < rotas.size(); i++) {
            if (rotas.get(i).getNome().equals(origem)) {
                proximaProvincia.add(rotas.get(i).getDivisa().getProvincia().getNome());
            }
        }

        for (int i = 0; i < proximaProvincia.size(); i++) {

            if (paradasAteProvincia <= paradas) {

                if (proximaProvincia.get(i).equals(destino)) {
                    if (paradasAteProvincia == paradas) {
                        quantidadeTrajetos++;
                    }
                }
                quantidadeTrajetos = obterQuantidadeTrajetosPorParadas(proximaProvincia.get(i), destino, paradas, paradasAteProvincia, quantidadeTrajetos);

            }
        }

        return quantidadeTrajetos;
    }

    /**
     * Metodo que obtem o menor trajeto entre duas provincias.
     *
     * @param origem
     * @param destino
     * @return
     */
    public int obterMenorTrajeto(String origem, String destino) {
        return obterMenorTrajeto(origem, destino, 0);
    }

    /**
     * Metodo que obtem o menor trajeto entre duas provincias. (Algoritmo de
     * Dijkstra)
     *
     * @param origem
     * @param destino
     * @param distancia
     * @return
     */
    private int obterMenorTrajeto(String origem, String destino, int distancia) {

        List<Integer> distanciasProximaProvincia = new LinkedList<>();
        List<String> proximaProvincia = new LinkedList<>();

        for (int i = 0; i < rotas.size(); i++) {
            if (rotas.get(i).getNome().equals(origem)) {
                distanciasProximaProvincia.add(rotas.get(i).getDivisa().getDistancia());
                proximaProvincia.add(rotas.get(i).getDivisa().getProvincia().getNome());
            }
        }

        int distanciaProvinciaMaisPerto = distancia + distanciasProximaProvincia.get(0);
        String provinciaMaisPerto = proximaProvincia.get(0);
        for (int i = 1; i < distanciasProximaProvincia.size(); i++) {
            if (distanciasProximaProvincia.get(i) == distanciaProvinciaMaisPerto) {

                int distanciaRealProvinciaI = obterMenorTrajeto(proximaProvincia.get(i), destino, distancia + distanciasProximaProvincia.get(i));
                int distanciaRealProvinciaMaisPerto = obterMenorTrajeto(provinciaMaisPerto, destino, distancia + distanciaProvinciaMaisPerto);

                if (distanciaRealProvinciaI <= distanciaRealProvinciaMaisPerto) {
                    distanciaProvinciaMaisPerto = distancia + distanciasProximaProvincia.get(i);
                    provinciaMaisPerto = proximaProvincia.get(i);
                }

            } else if (distanciasProximaProvincia.get(i) < distanciaProvinciaMaisPerto) {

                distanciaProvinciaMaisPerto = distancia + distanciasProximaProvincia.get(i);
                provinciaMaisPerto = proximaProvincia.get(i);

            }
        }

        if (provinciaMaisPerto.equals(destino)) {
            return distanciaProvinciaMaisPerto;
        } else {
            return obterMenorTrajeto(provinciaMaisPerto, destino, distanciaProvinciaMaisPerto);
        }

    }

    /**
     * Metodo que obtem a quantidade de rotas por distancia maxima.
     *
     * @param origem
     * @param destino
     * @param distanciaMaxima
     * @return
     */
    public int obterQuantidadeDeRotasMaximaDistancia(String origem, String destino, int distanciaMaxima) {
        int distancia = 0;
        int quantidade = 0;
        return obterQuantidadeDeRotasMaximaDistancia(origem, destino, distancia, quantidade, distanciaMaxima);
    }

    /**
     * Metodo que obtem a quanidade de rotas por distancia maxima. (Algoritmo)
     *
     * @param origem
     * @param destino
     * @param distancia
     * @param quantidade
     * @param distanciaMaxima
     * @return
     */
    private int obterQuantidadeDeRotasMaximaDistancia(String origem, String destino, int distancia, int quantidade, int distanciaMaxima) {

        List<Integer> distanciasProximaProvincia = new LinkedList<>();
        List<String> proximaProvincia = new LinkedList<>();

        for (int i = 0; i < rotas.size(); i++) {
            if (rotas.get(i).getNome().equals(origem)) {
                distanciasProximaProvincia.add(rotas.get(i).getDivisa().getDistancia());
                proximaProvincia.add(rotas.get(i).getDivisa().getProvincia().getNome());
            }
        }

        for (int i = 0; i < distanciasProximaProvincia.size(); i++) {

            if (distancia + distanciasProximaProvincia.get(i) < distanciaMaxima) {
                if (proximaProvincia.get(i).equals(destino)) {
                    quantidade++;
                }
                quantidade = obterQuantidadeDeRotasMaximaDistancia(proximaProvincia.get(i), destino, distancia + distanciasProximaProvincia.get(i), quantidade, distanciaMaxima);

            }
        }

        return quantidade;

    }

}
