/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import malha.Malha;

/**
 *
 * @author Matheus
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");

        System.out.println("#1: " + malha.calcularDistancia("A-B-C"));

        System.out.println("#2: " + malha.calcularDistancia("A-D"));

        System.out.println("#3: " + malha.calcularDistancia("A-D-C"));

        System.out.println("#4: " + malha.calcularDistancia("A-E-B-C-D"));

        System.out.println("#5: " + malha.calcularDistancia("A-E-D"));

        System.out.println("#6: " + malha.obterQuantidadeTrajetosPorParadasMaxima("C", "C", 3));

        System.out.println("#7: " + malha.obterQuantidadeTrajetosPorParadas("A", "C", 4));

        System.out.println("#8: " + malha.obterMenorTrajeto("A", "C"));

        System.out.println("#9: " + malha.obterMenorTrajeto("B", "B"));

        System.out.println("#10: " + malha.obterQuantidadeDeRotasMaximaDistancia("C", "C", 30));

    }

}
