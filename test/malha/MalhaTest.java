/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package malha;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matheus
 */
public class MalhaTest {

    public MalhaTest() {
    }

    @Test
    public void testInserirMalha() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals(malha.getRotas().get(0).getNome(), "B");
    }

    @Test
    public void testCalcularDistancia() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals("9", malha.calcularDistancia("A-B-C"));
        assertEquals("ROTA NAO EXISTE", malha.calcularDistancia("A-E-D"));
    }

    @Test
    public void testObterQuantidadeTrajetosPorParadasMaxima() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals(malha.obterQuantidadeTrajetosPorParadasMaxima("C", "C", 3), 2);
    }

    @Test
    public void testObterQuantidadeTrajetosPorParadas() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals(malha.obterQuantidadeTrajetosPorParadas("A", "C", 4), 3);
    }

    @Test
    public void testObterMenorTrajeto() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals(malha.obterMenorTrajeto("A", "C"), 9);
    }

    @Test
    public void testObterQuantidadeDeRotasMaximaDistancia() {
        Malha malha = new Malha();
        malha.inserirMalha("BC4, AD5, DC8, CD8, DE6, CE2, AE7, EB3, AB5");
        assertEquals(malha.obterQuantidadeDeRotasMaximaDistancia("C", "C", 30), 7);
    }

}
